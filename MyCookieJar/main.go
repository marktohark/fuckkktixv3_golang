package MyCookieJar

import (
	"errors"
	"net/http"
	"net/http/cookiejar"
	"net/url"
)

type MyCookieJar_s struct {
	*cookiejar.Jar
}

func (this *MyCookieJar_s)GetCookie(theUrl string, cookieName string) (*http.Cookie, error) {
	_url, err := url.Parse(theUrl)
	if  err != nil {

		return nil,err
	}
	for _, cookie := range this.Cookies(_url) {
		if(cookie.Name == cookieName) {
			return cookie, nil
		}
	}
	return nil, errors.New("cannot found cookie name")
}

func New(o *cookiejar.Options) (*MyCookieJar_s, error) {
	jar,err := cookiejar.New(o)
	if(err != nil) {
		return nil,err
	}
	var myCookieJar = &MyCookieJar_s{
		Jar: jar,
	}
	return myCookieJar, nil
}
