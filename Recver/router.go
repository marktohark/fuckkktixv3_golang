package Recver

import (
	"FuckKKtixV3/Repository"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
)

func routerRegister(callback func([]string, http.ResponseWriter)) {
	http.HandleFunc("/api", func(writer http.ResponseWriter, request *http.Request) {
		if request.Method != "POST" {
			http.Error(writer, "404 not found.", http.StatusNotFound)
			return
		}
		decoder := json.NewDecoder(request.Body)
		var args []string
		err := decoder.Decode(&args)
		if err != nil {
			http.Error(writer, "json decode fail", http.StatusBadRequest)
			return
		}
		callback(args, writer)
		return
	})

	http.HandleFunc("/query", func(writer http.ResponseWriter, request *http.Request) {
		if request.Method != "GET" {
			http.Error(writer, "404 not found.", http.StatusNotFound)
			return
		}
		statusArr := Repository.GetAllStatus()
		var result string
		for i, status := range statusArr {
			result += strconv.Itoa(i) + ".<br />" + status.UserName + "<br />" + status.Session + "<br />" + status.Url + "<br><br />"
		}
		writer.Header().Set("Content-Type", "text/html; charset=utf-8")
		writer.Write([]byte(result))
	})
	http.HandleFunc("/captcha", func(writer http.ResponseWriter, request *http.Request) {
		if request.Method != "GET" {
			http.Error(writer, "404 not found.", http.StatusNotFound)
			return
		}

		data, err := ioutil.ReadFile("./src/captcha.html")
		if err != nil {
			http.Error(writer, err.Error(), http.StatusNotFound)
			return
		}
		writer.Write(data)
		writer.WriteHeader(200)
	})

	http.HandleFunc("/add", func(writer http.ResponseWriter, request *http.Request) {
		if request.Method != "GET" {
			http.Error(writer, "404 not found.", http.StatusNotFound)
			return
		}

		data, err := ioutil.ReadFile("./src/addAct.html")
		if err != nil {
			http.Error(writer, err.Error(), http.StatusNotFound)
			return
		}
		writer.Write(data)
		writer.WriteHeader(200)
	})
}