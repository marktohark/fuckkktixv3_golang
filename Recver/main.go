package Recver

import (
	"fmt"
	"net/http"
	"strconv"
)

type Recver_s struct {
}

func (this *Recver_s) Listen(port int, callback func([]string, http.ResponseWriter)) {
	routerRegister(callback)
	fmt.Println(":" + strconv.Itoa(port))
	http.ListenAndServe(":" + strconv.Itoa(port), nil)
}

func New() *Recver_s {
	return &Recver_s{}
}