package Migrate

import "github.com/jinzhu/gorm"

type TixSession struct {
	UserName string
	Session string
	Url string
}

func Migrate(DB *gorm.DB) {
	DB.AutoMigrate(TixSession{})
}