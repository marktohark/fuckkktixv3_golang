package main

import (
	"FuckKKtixV3/Cmder"
	"FuckKKtixV3/KKtixApi"
	"FuckKKtixV3/KKtixApi/CaptchaMgr"
	"FuckKKtixV3/KKtixApi/TixInfo"
	"FuckKKtixV3/Migrate"
	"FuckKKtixV3/Repository"
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

func _newGrabTix(param *Cmder.Param_s, w http.ResponseWriter) {
	errFunc := func(Msg string, w http.ResponseWriter) {
		w.Write([]byte(Msg))
		w.WriteHeader(404)
	}
	info := strings.Split(param.GrabInfo, ",")

	if len(info) != 5 {
		errFunc("data format error", w)
		return
	}
	actId := param.ActId
	userName := info[0]
	passwd := info[1]
	tixId, err := strconv.Atoi(info[2])
	if err != nil {
		errFunc(err.Error(), w)
		return
	}
	tixCount, err := strconv.Atoi(info[3])
	if err != nil {
		errFunc(err.Error(), w)
		return
	}
	threadCount, err := strconv.Atoi(info[4])
	if err != nil {
		errFunc(err.Error(), w)
		return
	}

	for i := 0; i < threadCount; i++ {
		go _grabTix(actId, userName, passwd, tixId, tixCount)
	}
}

func _grabTix(actId, _userName, _passwd string, tixId, tixCount int) {
	var pCapt *CaptchaMgr.CaptchaMgr_s
	pCapt, ok := g_cpatchaObj[actId]
	if !ok {
		pCapt = CaptchaMgr.New()
		g_cpatchaObj[actId] = pCapt
	}
	api, err := KKtixApi.New(pCapt)

	if err != nil {
		fmt.Println(err)
		return
	}
	userName, err := api.Login(_userName, _passwd)
	if err != nil {
		fmt.Println(err)
		return
	}

	api.UpdateActivityName(actId)
	var tixInfoArr []TixInfo.TixInfo_s
	tixInfoArr = append(tixInfoArr, *TixInfo.New(tixId, tixCount))

	api.SetTix(&tixInfoArr, nil)

	hasCorrect := false
	captMgr := api.GetCaptMgr()
	for {
		var captTemp string
		if !hasCorrect && captMgr.HasAnswers() {
			hasCorrect = captMgr.HasCorrect()
			if hasCorrect {
				captTemp = captMgr.GetCorrect()
				api.SetTix(&tixInfoArr, &captTemp)
			} else {
				captTemp = captMgr.GetOne()
				api.SetTix(&tixInfoArr, &captTemp)
			}
		}

		queueToken, err := api.GetQueueToken()
		if err != nil {
			fmt.Println(err)
			continue
		}
		buyToken,err := api.GetBuyTixToken(*queueToken)
		if err != nil {
			fmt.Println(err)
			continue
		}
		session := api.GetSession()
		if session == nil {
			continue
		}
		Repository.SetStatus(&Migrate.TixSession{
			Session: *session,
			UserName: userName,
			Url: api.MakeBuyUrl(*buyToken),
		})
		if captMgr.HasAnswers() {
			captMgr.SetCorrect(captTemp)
		}
	}
}