package KKtixApi

func (this *KKtixApi_s)MakeTixPostUrl(csrf_token string) string {
	return "https://queue.kktix.com/queue/" + this.m_activityName + "?authenticity_token=" + csrf_token
}

func (this *KKtixApi_s)MakeQueueUrl(token string) string {
	return "https://queue.kktix.com/queue/token/" + token
}

func (this *KKtixApi_s)MakeBuyUrl(number string) string {
	return "https://kktix.com/events/" + this.m_activityName + "/registrations/" + number
}