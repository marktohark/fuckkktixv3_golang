package TixForm

import "FuckKKtixV3/KKtixApi/TixInfo"

type TixForm_s struct {
	AgreeTerm bool `json:"agreeTerm"`
	Currency string `json:"currency"`
	Recaptcha map[string]interface{} `json:"recaptcha"`
	Tickets *[]TixInfo.TixInfo_s `json:"tickets"`
}

func New(tixInfoArr *[]TixInfo.TixInfo_s) (*TixForm_s) {
	return &TixForm_s{
		AgreeTerm: true,
		Currency: "TWD",
		Recaptcha: map[string]interface{}{},
		Tickets: tixInfoArr,
	}
}