package KKtixApi

import (
	"FuckKKtixV3/KKtixApi/CaptchaMgr"
	"FuckKKtixV3/MyCookieJar"
	"FuckKKtixV3/KKtixApi/TixForm"
	"FuckKKtixV3/KKtixApi/TixInfo"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

const g_url = "https://kktix.com"
const g_csrfTokenUrl = g_url + "/events"
const g_loginUrl = g_url + "/users/sign_in"
const g_logoutUrl = g_url + "/users/sign_out"

type KKtixApi_s struct {
	m_http *http.Client
	m_cookieJar *MyCookieJar.MyCookieJar_s
	m_activityName string
	m_tixForm *TixForm.TixForm_s
	m_tixFormJson []byte
	m_captchaMgr *CaptchaMgr.CaptchaMgr_s
}

func New(capt *CaptchaMgr.CaptchaMgr_s) (*KKtixApi_s, error) {
	var api = &KKtixApi_s{
		m_captchaMgr: capt,
	}
	err := api.Init()
	if err != nil {
		return nil, err
	}
	return api, nil
}

func (this *KKtixApi_s) Init() error {
	//fiddler debug
	httpTransport := &http.Transport{
	Proxy: func(r *http.Request) (*url.URL, error) { return url.Parse("http://127.0.0.1:8888") }, //
	}
	//fiddler debug
	jar, err := MyCookieJar.New(nil)
	if(err != nil) {
		return err
	}
	this.m_cookieJar = jar
	this.m_http = &http.Client {
		Transport: httpTransport, 	//fiddler debug
		Jar: this.m_cookieJar,
	}
	return nil
}

func (this *KKtixApi_s) GetCSRF_Token(getNew bool) (*string, error) {
	var cookie *http.Cookie
	var err error
	if getNew {
		resp, err := this.m_http.Get(g_csrfTokenUrl)
		if(err != nil) {
			return nil, err
		}
		resp.Body.Close()
	}

	cookie, err = this.m_cookieJar.GetCookie(g_url, "XSRF-TOKEN")
	if(err != nil) {
		return nil, err
	}
	return &cookie.Value, nil
}

func (this *KKtixApi_s) Login(userName, passwd string) (string, error) {
	csrf_token, err := this.GetCSRF_Token(true)
	if err != nil {
		return "", err
	}

	*csrf_token, err = url.QueryUnescape(*csrf_token)
	if err != nil {
		return "", err
	}

	resp, err := this.m_http.PostForm(g_loginUrl, url.Values{
		"utf8": {"\xe2\x9c\x93"},
		"authenticity_token": {*csrf_token},
		"user[login]": {userName},
		"user[password]": {passwd},
		"user[remember_me]": {"1"},
		"commit": {"\xE7\x99\xBB\xE5\x85\xA5"},
	})
	if err != nil {
		return "", err
	}
	resp.Body.Close()
	cookie, err := this.m_cookieJar.GetCookie(g_url, "user_display_name_v2")
	if(err != nil) {
		return "", err
	}
	return cookie.Value, nil
}

func (this *KKtixApi_s) Logout() (error) {
	csrf_token, err := this.GetCSRF_Token(true)
	if err != nil {
		return err
	}

	*csrf_token, err = url.QueryUnescape(*csrf_token)
	if err != nil {
		return err
	}

	resp, err := this.m_http.PostForm(g_logoutUrl, url.Values{
		"_method": {"delete"},
		"authenticity_token": {*csrf_token},
	})
	if err != nil {
		return err
	}
	resp.Body.Close()
	_, err = this.m_cookieJar.GetCookie(g_url, "user_display_name_v2")
	if err == nil { //can't find cookie is correct
		return errors.New("logout fail")
	}
	return nil
}

func (this *KKtixApi_s) Register(gmailAccount string, prefixUserName string, number int, passwd string) (*string,*string,error) {
	//return userName,passwd,err
	var userName = fmt.Sprintf("%s%d", prefixUserName, number)
	csrf_token, err := this.GetCSRF_Token(true)
	if err != nil {
		return nil, nil, err
	}
	*csrf_token, err = url.QueryUnescape(*csrf_token)
	resp, err := this.m_http.PostForm("https://kktix.com/users", url.Values{
		"utf8": {"\xe2\x9c\x93"},
		"authenticity_token": {*csrf_token},
		"user[login]": {userName},
		"user[email]": {fmt.Sprintf("%s+%d@gmail.com", gmailAccount, number)},
		"user[password]": {passwd},
		"user[password_confirmation]": {passwd},
		"commit": {"\xE8\xA8\xBB\xE5\x86\x8A"},
	})
	if err != nil {
		return nil, nil, err
	}
	resp.Body.Close()
	cookie, err := this.m_cookieJar.GetCookie(g_url, "user_display_name_v2")
	if err != nil {
		return nil, nil, err
	}
	if cookie.Value != userName {
		return nil, nil, errors.New("account isn't the same")
	}
	return &userName, &passwd, nil
}

func (this *KKtixApi_s) UpdateActivityName(activityName string) {
	this.m_activityName = activityName
}

func (this *KKtixApi_s) SetTix(tixInfoArr *[]TixInfo.TixInfo_s, captStr *string) (error) {
	this.m_tixForm = TixForm.New(tixInfoArr)
	var tixForm interface{}
	if captStr != nil {
		tixForm = struct {
			*TixForm.TixForm_s
			CaptchaStr string `json:"custom_captcha"`
		}{
			this.m_tixForm,
			*captStr,
		}
	} else {
		tixForm = this.m_tixForm
	}
	jsonStr, err := json.Marshal(tixForm)
	if err != nil {
		return err
	}
	this.m_tixFormJson = jsonStr
	return nil
}

func (this *KKtixApi_s) GetQueueToken() (*string, error) {
	csrfToke, err := this.GetCSRF_Token(false)
	if err != nil {
		return nil, err
	}
	ioR := bytes.NewReader(this.m_tixFormJson)
	resp, err := this.m_http.Post(this.MakeTixPostUrl(*csrfToke), "application/json", ioR)
	if err != nil {
		return nil, err
	}
	/*if resp.StatusCode != 200 {
		resp.Body.Close()
		return nil, errors.New(fmt.Sprintf("GetQueueToken: http code is %d", resp.StatusCode))
	}*/
	respJson, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	resp.Body.Close()
	var queueTokenJson map[string]interface{}
	if err := json.Unmarshal(respJson, &queueTokenJson); err != nil {
		return nil, err
	}
	queueToken, exist := queueTokenJson["token"]
	if !exist {
		result, exist := queueTokenJson["result"]
		if !exist {
			return nil, errors.New("unknowMsg:" + string(respJson))
		}
		var result_str = result.(string)
		switch result_str {
		case "event_not_yet_start":
			return nil, errors.New("activity no start")
		case "Can't verify CSRF token":
			return nil, errors.New("csrf_token expire")
		case "Enqueue too soon!":
			return nil, errors.New("post is fast")
		case "TICKET_SOLD_OUT":
			return nil, errors.New("tix is zero")
		case "CAPTCHA_WRONG_ANSWER":
			return nil, errors.New("captcha is error")
		default:
			return nil, errors.New("default: unknowMsg: " + result_str + ", http code: " + strconv.Itoa(resp.StatusCode))
		}
	}
	var result = queueToken.(string)
	return &result, nil
}

func (this *KKtixApi_s) GetBuyTixToken(queueToken string) (*string, error) {
	var result string
	for {
		resp, err := this.m_http.Get(this.MakeQueueUrl(queueToken))
		if err != nil {
			return nil, err
		}
		if resp.StatusCode != 200 {
			resp.Body.Close()
			return nil, errors.New(fmt.Sprintf("GetButTixToken: http code is %d", resp.StatusCode))
		}
		respByte, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		resp.Body.Close()

		var jsonObj map[string]interface{}
		err = json.Unmarshal(respByte, &jsonObj)
		if err != nil {
			return nil, errors.New("GetButTixToken: json fail")
		}

		value, exist := jsonObj["to_param"]
		if !exist {
			fmt.Println("GetButTixToken: unknowMsg: " + string(respByte))
			time.Sleep(100)
			continue
		}
		result = value.(string)
		break
	}

	return &result, nil
}

func (this *KKtixApi_s) GetSession() *string {
	cookie, err := this.m_cookieJar.GetCookie(g_url, "kktix_session_token_v2")
	if err != nil {
		return nil
	}
	return &cookie.Value
}

func (this *KKtixApi_s) GetCaptMgr() *CaptchaMgr.CaptchaMgr_s {
	return this.m_captchaMgr
}