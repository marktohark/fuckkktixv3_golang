package CaptchaMgr

import (
	"sync"
)

type CaptchaMgr_s struct {
	m_answers []string
	m_ansIndex int
	m_mutex sync.RWMutex
	m_mutex2 sync.RWMutex
	m_correct *string
}

func (this *CaptchaMgr_s) HasCorrect() bool {
	var result bool
	this.m_mutex2.RLock()
	result = this.m_correct != nil
	this.m_mutex2.RUnlock()
	return result
}

func (this *CaptchaMgr_s) SetCorrect(correct string) {
	this.m_mutex2.Lock()
	this.m_correct = &correct
	this.m_mutex2.Unlock()
}

func (this *CaptchaMgr_s) GetCorrect() string {
	var result string
	this.m_mutex2.RLock()
	result = *this.m_correct
	this.m_mutex2.RUnlock()
	return result
}

func (this *CaptchaMgr_s) HasAnswers() bool {
	this.m_mutex.RLock()
	defer this.m_mutex.RUnlock()
	var r = len(this.m_answers) != 0
	return r
}

func (this *CaptchaMgr_s) GetOne() string {
	this.m_mutex.RLock()
	defer this.m_mutex.RUnlock()

	if len(this.m_answers) == 0 {
		return ""
	}
	var result = this.m_answers[this.m_ansIndex]
	this.m_ansIndex = (this.m_ansIndex + 1) % len(this.m_answers)
	return result
}

func (this *CaptchaMgr_s) AddOne(ans string) {
	if len(ans) == 0 {
		return
	}

	this.m_mutex.Lock()
	this.m_answers = append(this.m_answers, ans)
	this.m_mutex.Unlock()
}

func New() *CaptchaMgr_s {
	return &CaptchaMgr_s{
		m_ansIndex: 0,
		m_answers: []string{},
	}
}