package TixInfo

type TixInfo_s struct {
	Id int `json:"id"`
	InvitationCodes []string `json:"invitationCodes"`
	Member_code string `json:"member_code"`
	Quantity int `json:"quantity"`
	Use_qualification_id *string `json:"use_qualification_id"`
}

func New(id int, count int) (*TixInfo_s) {
	return &TixInfo_s{
		Id:id,
		InvitationCodes: []string{},
		Member_code: "",
		Quantity: count,
		Use_qualification_id: nil,
	}
}
