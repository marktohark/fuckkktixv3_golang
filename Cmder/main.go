package Cmder

import (
	"flag"
	"os"
)

type Cmder_s struct {
	param Param_s
}

func (this *Cmder_s) Parse(args []string) (*Param_s, error) {
	err := flag.CommandLine.Parse(args)
	if err != nil {
		return nil, err
	}
	flag.CommandLine = flag.NewFlagSet(os.Args[0], flag.ExitOnError)
	return &this.param, nil
}

func (this *Cmder_s) initFlag() {
	flag.StringVar(&this.param.GrabInfo, "grabinfo", "", "grabInfo ex: account,pwd,tixId,tixCount,threadCount")
	flag.StringVar(&this.param.AddCaptStr, "addcaptstr", "", "add cpatcha str")
	flag.StringVar(&this.param.ActId, "actid", "", "act id")
}

func New() *Cmder_s {
	var cmder = &Cmder_s{}
	cmder.initFlag()
	return cmder
}

